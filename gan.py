

class GenerativeAdversarialNetwork:
    """
     A Generative Adversarial Network is a tuple (D,G,P,A)
     where
     D is model for a D,
     G is a model for a G,
     P is a function that generates random seeds,
     A is a A incl. hyperparameters
    """
    def __init__(self, D, G, P, A):
        self.D = D
        self.G = G
        self.P = P
        self.A = A
    
    def train(self, X, verbose=True):
        """
         This function executes trainingalgorithm A with G and D as input.
        """
        self.A.set_G(self.G)
        self.A.set_D(self.D)
        self.A.set_P(self.P)
        self.G, self.D = self.A.run(X, verbose=verbose)

    def sample(self, n=1):
        """
         This function uses the seed generating function to generate samples.
        """
        return self.G.predict(self.P(n))