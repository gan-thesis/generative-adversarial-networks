import json
import numpy as np
import os

def setup(D, G, P_U_config, P_X_config, _lambda, m, path="evaluations/sample/"):
    """
     Store the setup of Generator, Discriminator, P^U, P^X and _lambda
     as a json file.
    """
    setup_json = dict()
    # Add Generator & Discriminator Model Configuration
    setup_json["G"] = G.to_json()
    setup_json["D"] = D.to_json()
    # Add other configuration dictionaries.
    setup_json["P_U"] = P_U_config
    setup_json["P_X"] = P_X_config
    setup_json["lambda"] = _lambda
    setup_json["m"] = m
    # Save File
    if not os.path.isdir(path): os.makedirs(path, exist_ok=True)
    with open(path+"setup.json", "w") as fp:
        json.dump(setup_json, fp, indent=2)

def data(X, path="evaluations/sample/"):
    data = X.tolist()
    with open(path+"data.json", "w") as fp:
        json.dump(data, fp, indent=2)

def training(A, path="evaluations/sample/"):
    """
     Store empiric risks of Generator and Discriminator.
    """
    # Store Risks
    training_json = dict()
    # Add measured risks
    training_json["R_d"] = [float(r.numpy()) for r in A.log["R_d"] ]
    training_json["R_g"] = [float(r.numpy()) for r in A.log["R_g"] ]
    # Save File
    if not os.path.isdir(path): os.makedirs(path, exist_ok=True)
    with open(path+"training.json", "w") as fp:
        json.dump(training_json, fp, indent=2)
    # Store Samples
    training_samples_arr = np.array(A.log["samples"])
    np.save(path+"training_samples.npy", training_samples_arr)
    # Store Labels & Predicted Labels
    y_true = np.array(A.log["y_true"])
    y_pred = np.array(A.log["y_pred"])
    y_true_and_pred = np.stack([y_true, y_pred], axis=-1)
    np.save(path+"y_true_and_pred.npy", y_true_and_pred)

def models(gan, path="evaluations/sample/"):
    """
     Store the trained Generator & Discriminator
    """
    if not os.path.isdir(path): os.makedirs(path, exist_ok=True)
    gan.G.save(path+"generator")
    gan.D.save(path+"discriminator")

def samples(G, P_U, n_mc=100000, path="evaluations/sample/"):
    """
     Store n_mc samples of generated data.
    """
    if not os.path.isdir(path): os.makedirs(path, exist_ok=True)
    U = P_U(n_mc)
    samples = G(U, training=False)
    # Save to file
    np.save(path+"samples.npy", samples)