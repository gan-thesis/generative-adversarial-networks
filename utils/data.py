import numpy as np

def h(g, x, u, y):
    """
     Returns a vectorized function that maps
     (x,u,y) -> x or g(u) depending on y.
    """
    hg = np.zeros_like(x)
    hg[y==1] = x[y==1]
    hg[y==0] = g(u[y==0], training=False)
    return hg