import numpy as np

def get_distribution(fnc_name, params, dim):
    # define a "complex"-custom distribution
    def custom(n):
        X_1 = np.random.normal(-2,0.3,size=(n,dim))
        X_2 = np.random.normal(4,2,size=(n,dim))
        X_3 = np.random.uniform(-6,4,size=(n,dim))
        X = np.concatenate([X_1,X_2,X_3])
        indices = np.random.randint(0,3*n,size=n)
        return X[indices]
    # define a multimodal distribution
    def multimodal(n):
        X_1 = np.random.normal(-2,1,size=(n,dim))
        X_2 = np.random.normal(2,1,size=(n,dim))
        X = np.concatenate([X_1,X_2])
        indices = np.random.randint(0,2*n,size=n)
        return X[indices]
    # get standard numpy distributions
    def P(n):
        if fnc_name == "multivariate_normal":
            return np.random.multivariate_normal(**params, size=n)
        else:
            fnc = getattr(np.random, fnc_name)
            return fnc(**params,size=(n,dim))

    if fnc_name == "custom":
        return custom
    elif fnc_name == "multimodal":
        return multimodal
    else:
        return P
