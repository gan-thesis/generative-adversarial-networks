# Generative Adversarial Networks

Implementation for Bachelor-Thesis.

## Setup

Setup the folder structure like this:

```
|-generative_adversarial_networks
 |-you are here
 |-venv/
 |-README.md
 |-requirements.txt
 |-.gitignore
 |-gan.py
 |-samples/
 |-eval/
 |-training/
 |-utils/
```

by cloning the project and creating a venv, to install the requirements into your virtual environment run:

```
pip install -r requirements.txt
```

## Sample-Usage

There are 3 files with the prefix 'sample' in the 'samples/' folder.

These are given as a first starting point on how to use this project, and can be run with:

```
python -m samples.sample_handcrafted
python -m samples.sample_create_config
python -m samples.sample_from_config
```

## Important Files

After review regarding variance of starting initialization, the old simulation & config files
have been moved into `unused_files/` while the following files are taking their place for the final experiments.

### Config-Creation

```
simuliations_create_config_post_review.py
```

### Simuliation

```
simuliations_create_config_post_review.py
```

### Prepare evaluation after training a GAN

```
eval/evaluate_review.py
```

### Analyse an experiement setup over all 50 repetitions

```
eval/evaluate-variants.ipynb
```

### Generate Excel-File over the experiments

This is the excel file `Auswertung.xlsx`

```
eval/mass_eval.py
```

### Analyse generated excel file

```
eval/evaluate-variants.ipynb
```

## Packaged-Library

Coming later.