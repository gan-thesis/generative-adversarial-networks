import json
import numpy as np
import tensorflow as tf

# Define the possible data distributions
data_distribution = dict(fnc_name="multimodal",params=None,dim=1)

G_large = tf.keras.Sequential()
G_large.add(tf.keras.layers.Dense(units=100, activation="relu"))
G_large.add(tf.keras.layers.Dense(units=100, activation="relu"))
G_large.add(tf.keras.layers.Dense(units=100, activation="relu"))
G_large.add(tf.keras.layers.Dense(units=100, activation="relu"))
G_large.add(tf.keras.layers.Dense(units=1))
G_large.build(input_shape=(None,10))
P_U_large = dict(fnc_name="multivariate_normal", params=dict(mean=[0 for _ in range(10)], cov=np.eye(10).tolist()), dim=10)

# the model-space for the discriminator
D = tf.keras.Sequential()
D.add(tf.keras.layers.Dense(units=125, activation="relu"))
D.add(tf.keras.layers.Dense(units=125, activation="relu"))
D.add(tf.keras.layers.Dense(units=1, activation="sigmoid"))
D.build(input_shape=(None,1))
D.summary()


for m in [25,50,75,100,500]:
    # set the hyper parameters as basically the same over all configurations, except the batchsize B
    # which we set B = m
    _lambda = dict(
        eta_g = 5e-3,
        eta_d = 5e-3,
        N = 5000,
        K = 100,
        B = m
    )
    # create the actual json config
    config = dict()
    config["name"] = "review-representative-m{}".format(m)
    # outer loop
    config["G"] = G_large.to_json()
    config["P_U"] = P_U_large
    config["D"] = D.to_json()
    # middle loop
    config["m"] = m
    # inner loop
    config["P_X"] = data_distribution
    # hyperparameters
    config["lambda"] = _lambda
    with open("configs/{}.json".format(config["name"]), "w") as fp:
        json.dump(config, fp, indent=2)