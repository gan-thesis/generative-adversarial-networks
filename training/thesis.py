
import numpy as np
import tensorflow as tf

from utils.data import h # the trainingfunction h_g with g as the first parameter

class Algorithm:
    """
     The trainingsalgorithm as presented in the bachelors thesis this library was written for.
    """
    def __init__(self, eta_g=1e-3, eta_d=1e-3, N=1, B=64, K=1, n_mc=500):
        """
         Initialize hyperparameters except for theta and phi, as those are given by the models set by their specific functions.
        """
        self.eta_g = eta_g
        self.eta_d = eta_d
        self.N = N
        self.B = B
        self.K = K
        # we want to keep logs of the training process
        self.n_mc = n_mc
        self.log = dict(R_d=[], R_g=[], y_true=[], y_pred=[], samples=[], weights=[])

    def set_G(self, G):
        self.theta = G
    
    def set_D(self, D):
        self.phi = D

    def set_P(self, P):
        self.P = P

    def end_of_epoch(self):
        # Log samples for later evaluations
        U = self.P(self.n_mc)
        samples = self.theta.predict(U)
        self.log["samples"].append(samples)
        # if our generator weights are just 2 dimensional
        # log generator weights for visualization of the training process
        weights = self.theta.get_weights()
        if len(weights) == 2:
            self.log["weights"].append((weights[0][0][0], weights[1][0]))

    def run(self, X, verbose=True):
        """
         Implements the full algorithm as given in the thesis.
        """
        self.theta_optimizer = tf.keras.optimizers.SGD(learning_rate=self.eta_g)
        self.phi_optimizer = tf.keras.optimizers.SGD(learning_rate=self.eta_d)
        self.binary_crossentropy = tf.keras.losses.BinaryCrossentropy()
        # Run the outer loop over e=1,...,N
        for e in range(1,self.N+1):
            if verbose:
                print("\nRunning outer loop e={}".format(e))
            # Update h_g (this is due to implementational restraints)
            self.outer_loop(X, verbose)
        # Return the fully trainined G and D
        return self.theta, self.phi

    def outer_loop(self, X, verbose):
        """
         The outer loop over e=1,...,N
        """
        # Update D K times
        for _ in range(1,self.K+1):
            if verbose:
                print("\rRunning inner loop {:2d}".format(_), end="")
            self.inner_loop(X, _)
        # Update G once
        U = self.P(self.B)
        R_g = self.update_theta(U)
        # store R_g for later evaluation
        self.log["R_g"].append(R_g)
        # Add end of epoch-logs
        self.end_of_epoch()

    def inner_loop(self, X, K):
        """
         The inner loop that is executed K times in which the D (phi) is updated.
        """
        U = self.P(self.B)
        Y = np.random.binomial(1,0.5,size=(self.B,1))
        k = np.random.randint(0,self.B,size=self.B)
        # For ease of implementation we are NOT stacking D here as (X_j,Y_j) tuples proposed in the algorithm
        D = h(self.theta, X[k], U, Y.flatten()) # we flatten Y to use it for boolean - indexing.
        R_d, y_true, y_pred = self.update_phi(D,Y)
        # Store R_d and (y_true, y_pred) for later evaluations.
        self.log["R_d"].append(R_d)
        if K%2 == 0:
            self.log["y_true"].append(y_true)
            self.log["y_pred"].append(y_pred)

    @tf.function
    def update_phi(self, D, Y):
        """
         To update phi, we are not updating it directly via the gradient,
         but instead we are using the tensorflow SGD optimizer.
        """
        # Use a gradient tape for automatic backpropagation
        with tf.GradientTape() as tape:
            # Use the current D, to predict the labels
            y_pred = self.phi(D, training=True)
            # Now calculate the empirical risk via binary crossentropy
            R_emp = self.binary_crossentropy(Y, y_pred)
        # Calculate the gradients
        gradients = tape.gradient(R_emp, self.phi.trainable_variables)
        # And update the value for phi
        self.phi_optimizer.apply_gradients(zip(gradients, self.phi.trainable_variables))
        # return R, y_true, y_pred for later evaluation
        return R_emp, Y, y_pred

    @tf.function
    def update_theta(self, U):
        """
         To update theta we are not updating it directly via the gradient,
         but instead we are using the tensorflow SGD optimizer.
        """
        # Use a gradient tape for automatic backpropagation
        with tf.GradientTape() as tape:
            # Generate samples that we feed into the D
            samples = self.theta(U, training=True)
            labels = self.phi(samples, training=False)
            # Calculate the empirical risk via binary crossentropy
            # with the goal of having all ones as labels.
            R_emp = self.binary_crossentropy(tf.ones_like(labels), labels)
        # Calculate the gradients
        gradients = tape.gradient(R_emp, self.theta.trainable_variables)
        # And update the value for theta
        self.theta_optimizer.apply_gradients(zip(gradients, self.theta.trainable_variables))
        return R_emp