import json
import pprint
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import tensorflow as tf
import os
import utils.dist

def training_data(model_folder, variant):
    print("Saving empiric CDF")
    # Set distributions
    with open("evaluation/{}/{}/data.json".format(model_folder, variant),"r") as fp:
        data = json.load(fp)
    data = np.array(data).ravel()
    # Calculate cdf
    # get the lowest value for both sample sets
    min_val = np.min(data)
    # get the ighest value for both sample sets
    max_val = np.max(data)
    # get a "grid" between those 2
    grid = np.arange(min_val, max_val, (max_val-min_val)/1000)
    # calculate the cdf
    cdf = [data[data < x].shape[0]/data.shape[0] for x in grid]
    return data.tolist(), cdf

def model_cdf(model_folder, variant, N_MC = 100000):
    print("Saving CDF")
    # Set distributions
    with open("evaluation/{}/{}/setup.json".format(model_folder, variant),"r") as fp:
        setup = json.load(fp)
    P_X = utils.dist.get_distribution(**setup["P_X"])
    P_U = utils.dist.get_distribution(**setup["P_U"])
    # Generate Samples
    G = tf.keras.models.load_model("evaluation/{}/{}/generator".format(model_folder, variant))
    X_mc = P_X(N_MC)
    X_gen = G.predict(P_U(N_MC))
    # Calculate cdf
    # get the lowest value for both sample sets
    min_val = min(
        np.min(X_mc),
        np.min(X_gen)
    )
    # get the ighest value for both sample sets
    max_val = max(
        np.max(X_mc),
        np.max(X_gen)
    )
    # get a "grid" between those 2
    grid = np.arange(min_val, max_val, (max_val-min_val)/1000)
    # calculate the cdf
    cdf_true = [X_mc[X_mc < x].shape[0]/N_MC for x in grid]
    cdf_generated = [X_gen[X_gen < x].shape[0]/N_MC for x in grid]
    return X_mc.tolist(), X_gen.tolist(), cdf_true, cdf_generated

def evaluate_variant(model_folder, variant):
    variant_data = dict()
    # Add samples
    empiric, empiric_cdf = training_data(model_folder, variant)
    data_samples, generator_samples, cdf_true, cdf_generated = model_cdf(model_folder, variant)
    variant_data["Samples"] = dict(
        empiric=empiric,
        data=data_samples,
        generator=generator_samples
    )
    variant_data["CDF"] = dict(
        empiric_cdf = empiric_cdf,
        data = cdf_true,
        generated = cdf_generated
    )
    return variant_data

def evaluate_folder(model_folder):
    model_data = dict()
    # Get variants of the model
    for variant in os.listdir("evaluation/"+model_folder):
        print("Evaluating", model_folder, variant)
        model_data[variant] = evaluate_variant(model_folder, variant)
    with open("evaluated/{}.json".format(model_folder),"w") as fp:
        json.dump(model_data, fp)

# Create the folder for storing evaluated data
if not os.path.isdir("evaluated"):
    os.mkdir("evaluated")

# Get list of data that has to be evaluated
for model_folder in os.listdir("evaluation/"):
    if "review" not in model_folder: continue
    evaluate_folder(model_folder)