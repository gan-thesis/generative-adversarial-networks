import matplotlib.pyplot as pyplot
import numpy as np

def discriminator_risk(A, plot_object=None):
    # If we have no plot passed, set plot
    if plot_object is not None:
        plt = plot_object
    else:
        plt = pyplot
    # add the discrimintor risk measurements to the plot
    plt.title("Discriminator risk measurements")
    plt.plot(A.log["R_d"], label="R_emp of the discriminator")
    # add vertical lines at each start of a new epoch
    plt.vlines(x=[e*A.K+A.K for e in range(A.N-1)], ymin=min(A.log["R_d"]), ymax=max(A.log["R_d"]), color="red", label="Start of new epoch")
    # add labels to plot
    plt.legend()
    # and either show it directly, or return it for further processing.
    if plot_object is None:
        plt.show()
    else:
        return plt

def generator_risk(A, plot_object=None):
    # If we have no plot passed, set plot
    if plot_object is not None:
        plt = plot_object
    else:
        plt = pyplot
    # add the generator risk measurements to the plot
    plt.title("Generator risk measurements")
    plt.plot(A.log["R_g"], label="R_emp of the generator")
    # add labels to plot
    plt.legend()
    # and either show it directly, or return it for further processing.
    if plot_object is None:
        plt.show()
    else:
        return plt

def final_distribution(A, X, plot_object=None):
    # If we have no plot passed, set plot
    if plot_object is not None:
        plt = plot_object
    else:
        plt = pyplot
    # add the generator risk measurements to the plot
    plt.title("Distributions at the end of training")
    plt.hist(X, bins=50, density=True, label="Data distribution")
    plt.hist(A.log["samples"][-1], bins=50, density=True, label="Generated distribution", alpha=0.5)
    # add labels to plot
    plt.legend()
    # and either show it directly, or return it for further processing.
    if plot_object is None:
        plt.show()
    else:
        return plt

def cumulative_distribution_metric(A, X, plot_object=None):
    # If we have no plot passed, set plot
    if plot_object is not None:
        plt = plot_object
    else:
        plt = pyplot
    # calculate the sup-difference of the distribution functions
    metric = []
    for sample in A.log["samples"]:
        min_value = min(min(X), min(sample))
        max_value = max(max(X), max(sample))
        x = np.arange(min_value, max_value, 0.01)
        # now calculate for each x_i in x, P(X<=x_i) and P(g(u)<=x_i)
        max_difference = 0
        for x_i in x:
            P_X = X[X <= x_i].shape[0]/X.shape[0]
            P_gU = sample[sample <= x_i].shape[0]/sample.shape[0]
            diff = abs(P_X-P_gU)
            # if this difference is greater than maximum known difference overwrite it
            if diff > max_difference:
                max_difference = diff
        # add this max-difference to the list of metric measurements
        metric.append(max_difference)
    # add the generator risk measurements to the plot
    plt.title("sup-Difference in the cumulative distribution functions")
    plt.plot(metric, label="sup |P[X<=x]-P[g(U)<=x]|")
    # add labels to plot
    plt.legend()
    # and either show it directly, or return it for further processing.
    if plot_object is None:
        plt.show()
    else:
        return plt

def training(A, X):
    # create plot object that we can pass around
    plt = pyplot
    plt.subplot(2,2,1)
    # create a subplot for the discriminator
    plt = discriminator_risk(A, plot_object=plt)
    # create a subplot for the generator
    plt.subplot(2,2,2)
    plt = generator_risk(A, plot_object=plt)
    # create a subplot for the final samples
    plt.subplot(2,2,3)
    plt = final_distribution(A, X, plot_object=plt)
    # create a subplot over the difference in the cumulative distribution function
    plt.subplot(2,2,4)
    plt = cumulative_distribution_metric(A, X, plot_object=plt)
    plt.show()

def weights(A):
    # create plot object that we can pass around
    plt = pyplot
    neurons = [weight[0] for weight in A.log["weights"]]
    biases = [weight[1] for weight in A.log["weights"]]
    # create a color map, that slowly fades in
    colors = ["#0000FF"+hex(55+round(200*i/len(A.log["weights"]))).split("x")[1] for i in range(len(A.log["weights"]))]
    # scatter the plots with fading in colors
    plt.scatter(x=neurons, y=biases, c=colors, s=1)
    plt.xlabel("Neuron-Value")
    plt.ylabel("Bias-Value")
    plt.show()