import json
import pprint
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import tensorflow as tf
import os

import utils.dist

def model_architecture(model_folder, variant):
    print("Saving Architecture")
    with open("evaluation/{}/{}/setup.json".format(model_folder, variant),"r") as fp:
        setup = json.load(fp)
    # Care, we had a bug in saving the model architectures in the wrong key.
    if "disc" not in model_folder:
        return setup["D"], setup["G"]
    else:
        return setup["G"], setup["D"]

def model_risk(model_folder, variant):
    print("Saving Risk")
    with open("evaluation/{}/{}/training.json".format(model_folder, variant),"r") as fp:
        training = json.load(fp)
    R_d, R_g = training["R_d"],  training["R_g"]
    return R_d, R_g
 
def model_metrics(model_folder, variant):
    print("Saving Metrics")
    y_true_and_pred = np.load("evaluation/{}/{}/y_true_and_pred.npy".format(model_folder, variant))
    # Parse
    y_true = y_true_and_pred[:,:,0,0]
    y_pred = y_true_and_pred[:,:,0,1]
    # Make predictions "binary"
    y_pred_binary = y_pred.round()
    # Calculate N, tp, ...
    N = y_true.shape[1]
    tp = np.sum(np.logical_and(y_true==1, y_pred_binary==1), axis=-1)
    tn = np.sum(np.logical_and(y_true==0, y_pred_binary==0), axis=-1)
    fp = np.sum(np.logical_and(y_true==0, y_pred_binary==1), axis=-1)
    fn = np.sum(np.logical_and(y_true==1, y_pred_binary==0), axis=-1)
    # Calculate metrics
    acc_chart = (tp+tn)/N
    prc_chart = tp/(tp+fp)
    rec_chart = tp/(tp+fn)
    spc_chart = tn/(tn+fp)
    return N, tp.tolist(), tn.tolist(), fp.tolist(), fn.tolist(), acc_chart.tolist(), prc_chart.tolist(), rec_chart.tolist(), spc_chart.tolist()

def model_cdf(model_folder, variant, N_MC = 100000):
    print("Saving CDF")
    # Set distributions
    with open("evaluation/{}/{}/setup.json".format(model_folder, variant),"r") as fp:
        setup = json.load(fp)
    P_X = utils.dist.get_distribution(**setup["P_X"])
    P_U = utils.dist.get_distribution(**setup["P_U"])
    # Generate Samples
    G = tf.keras.models.load_model("evaluation/{}/{}/generator".format(model_folder, variant))
    X_mc = P_X(N_MC)
    X_gen = G.predict(P_U(N_MC))
    # Calculate cdf
    # get the lowest value for both sample sets
    min_val = min(
        np.min(X_mc),
        np.min(X_gen)
    )
    # get the ighest value for both sample sets
    max_val = max(
        np.max(X_mc),
        np.max(X_gen)
    )
    # get a "grid" between those 2
    grid = np.arange(min_val, max_val, (max_val-min_val)/1000)
    # calculate the cdf
    cdf_true = [X_mc[X_mc < x].shape[0]/N_MC for x in grid]
    cdf_generated = [X_gen[X_gen < x].shape[0]/N_MC for x in grid]
    return X_mc.tolist(), X_gen.tolist(), cdf_true, cdf_generated

def evaluate_variant(model_folder, variant):
    variant_data = dict()
    # Load the model constructions
    variant_data["G"], variant_data["D"] = model_architecture(model_folder, variant)
    # Load the model risk
    R_d, R_g = model_risk(model_folder, variant)
    variant_data["Risk"] = dict(
        R_d = R_d,
        R_g = R_g,
    )
    # Calculate Classification Metrics
    N, tp, tn, fp, fn, acc_chart, prc_chart, rec_chart, spc_chart = model_metrics(model_folder, variant)
    variant_data["Classification"] = dict(
        N = N,
        tp = tp,
        tn = tn,
        fp = fp,
        fn = fn,
        acc_chart = acc_chart,
        prc_chart = prc_chart,
        rec_chart = rec_chart,
        spc_chart = spc_chart,
    )
    # Add samples
    data_samples, generator_samples, cdf_true, cdf_generated = model_cdf(model_folder, variant)
    variant_data["Samples"] = dict(
        data=data_samples,
        generator=generator_samples
    )
    variant_data["CDF"] = dict(
        data = cdf_true,
        generated = cdf_generated
    )
    return variant_data

def evaluate_folder(model_folder):
    model_data = dict()
    # Get variants of the model
    for variant in os.listdir("evaluation/"+model_folder):
        print("Evaluating", model_folder, variant)
        model_data[variant] = evaluate_variant(model_folder, variant)
    with open("evaluated/{}.json".format(model_folder),"w") as fp:
        json.dump(model_data, fp)

# Create the folder for storing evaluated data
if not os.path.isdir("evaluated"):
    os.mkdir("evaluated")

# Get list of data that has to be evaluated
for model_folder in os.listdir("evaluation/"):
    evaluate_folder(model_folder)