import json
import tensorflow as tf

# The Name of the setup
setup_name = "sample-config"

# The sample-size parameter m for (X_1,...,X_m) as sample of our distribution
m = 1000

# the model-space for the generator
G = tf.keras.Sequential()
G.add(tf.keras.layers.Dense(units=1))
G.build(input_shape=(None,1))
G.summary()

# the model-space for the discriminator
D = tf.keras.Sequential()
D.add(tf.keras.layers.Dense(units=25, activation="relu"))
D.add(tf.keras.layers.Dense(units=1, activation="sigmoid"))
D.build(input_shape=(None,1))
D.summary()

# the distribution P^U
P_U_config = dict(fnc_name="normal", params=dict(loc=0, scale=1), dim=1)

# the distribution P^X
P_X_config = dict(fnc_name="normal", params=dict(loc=1, scale=1), dim=1)

# the hyperparameters lambda
_lambda = dict(
    eta_g = 5e-3,
    eta_d = 5e-3,
    N = 1000,
    K = 50,
    B = 1000
)

# create the config json
config = dict()
config["name"] = setup_name
config["m"] = m
config["lambda"] = _lambda
config["P_U"] = P_U_config
config["P_X"] = P_X_config
config["G"] = G.to_json()
config["D"] = D.to_json()

with open("samples/config.json", "w") as fp:
    json.dump(config, fp, indent=2)