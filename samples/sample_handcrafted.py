import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

from gan import GenerativeAdversarialNetwork
from training import ThesisAlgorithm

from utils.dist import get_distribution
import utils.store as store

# The Name of the setup
setup_name = "sample-handcrafted"

# The sample-size parameter m for (X_1,...,X_m) as sample of our distribution
m = 1000

# the model-space for the generator
G = tf.keras.Sequential()
G.add(tf.keras.layers.Dense(units=1))
G.build(input_shape=(None,1))
G.summary()

# the model-space for the discriminator
D = tf.keras.Sequential()
D.add(tf.keras.layers.Dense(units=25, activation="relu"))
D.add(tf.keras.layers.Dense(units=1, activation="sigmoid"))
D.build(input_shape=(None,1))
D.summary()

# the distribution P^U
P_U_config = dict(fnc_name="normal", params=dict(loc=0, scale=1), dim=1)
P_U = get_distribution(**P_U_config)

# the distribution P^X
P_X_config = dict(fnc_name="normal", params=dict(loc=1, scale=1), dim=1)
P_X = get_distribution(**P_X_config)

# the hyperparameters lambda
_lambda = dict(
    eta_g = 5e-3,
    eta_d = 5e-3,
    N = 100,
    K = 50,
    B = 1000
)

# the trainingalgorithm initalized with hyperparameters
A = ThesisAlgorithm(**_lambda)

# create data X
X = P_X(m)

# define the GAN as a tuple (D,G,P,A)
gan = GenerativeAdversarialNetwork(D, G, P_U, A)

# Run training in a way, that we can cancel it anytime.
try:
    gan.train(X)
except KeyboardInterrupt:
    pass

# Store the full setup.
store.setup(G,D,P_U_config,P_X_config,_lambda,m, path="evaluation/{}/".format(setup_name))

# Store informations about the training-process
store.training(A, path="evaluation/{}/".format(setup_name))

# Store models
store.models(gan, path="evaluation/{}/".format(setup_name))

# Store samples of the trained Generator
store.samples(gan.G, P_U, path="evaluation/{}/".format(setup_name))