import json
import tensorflow as tf

from gan import GenerativeAdversarialNetwork
from training import ThesisAlgorithm

from utils.dist import get_distribution
import utils.store as store


# Load config
with open("samples/config.json") as fp:
    config = json.load(fp)

# the model-space for the generator
G = tf.keras.models.model_from_json(config["G"])

# the model-space for the discriminator
D = tf.keras.models.model_from_json(config["D"])

# the distribution P^U
P_U = get_distribution(**config["P_U"])

# the distribution P^X
P_X = get_distribution(**config["P_X"])

# the trainingalgorithm initalized with hyperparameters
A = ThesisAlgorithm(**config["lambda"])

# create data X
X = P_X(config["m"])

# define the GAN as a tuple (D,G,P,A)
gan = GenerativeAdversarialNetwork(D, G, P_U, A)

# Run training in a way, that we can cancel it anytime.
try:
    gan.train(X)
except KeyboardInterrupt:
    pass

# Store the full setup.
store.setup(G, D, config["P_U"], config["P_X"], config["lambda"], config["m"], path="evaluation/{}/".format(config["name"]))

# Store informations about the training-process
store.training(A, path="evaluation/{}/".format(config["name"]))

# Store models
store.models(gan, path="evaluation/{}/".format(config["name"]))

# Store samples of the trained Generator
store.samples(gan.G, P_U, path="evaluation/{}/".format(config["name"]))