import json
import numpy as np
import tensorflow as tf

# Define the possible values for m
m_values = [10,25,50,75,100,1000]

# Define the possible data distributions
data_distributions = [
    dict(fnc_name="normal", params=dict(loc=1, scale=1), dim=1),
    dict(fnc_name="uniform", params=dict(low=-2.5, high=5), dim=1),
    dict(fnc_name="custom",params=None,dim=1),
]

# Define the possible models
G_small = tf.keras.Sequential()
G_small.add(tf.keras.layers.Dense(units=1))
G_small.build(input_shape=(None,1))
P_U_small = dict(fnc_name="uniform", params=dict(low=0, high=1), dim=1)

G_medium = tf.keras.Sequential()
G_medium.add(tf.keras.layers.Dense(units=9, activation="relu"))
G_medium.add(tf.keras.layers.Dense(units=9, activation="relu"))
G_medium.add(tf.keras.layers.Dense(units=9, activation="relu"))
G_medium.add(tf.keras.layers.Dense(units=1))
G_medium.build(input_shape=(None,3))
P_U_medium = dict(fnc_name="multivariate_normal", params=dict(mean=[0,0,0], cov=np.eye(3).tolist()), dim=3)

G_large = tf.keras.Sequential()
G_large.add(tf.keras.layers.Dense(units=100, activation="relu"))
G_large.add(tf.keras.layers.Dense(units=100, activation="relu"))
G_large.add(tf.keras.layers.Dense(units=100, activation="relu"))
G_large.add(tf.keras.layers.Dense(units=100, activation="relu"))
G_large.add(tf.keras.layers.Dense(units=1))
G_large.build(input_shape=(None,10))
P_U_large = dict(fnc_name="uniform", params=dict(low=-1, high=1), dim=10)

# the model-space for the discriminator
D = tf.keras.Sequential()
D.add(tf.keras.layers.Dense(units=125, activation="relu"))
D.add(tf.keras.layers.Dense(units=125, activation="relu"))
D.add(tf.keras.layers.Dense(units=1, activation="sigmoid"))
D.build(input_shape=(None,1))
D.summary()

# Define the list of generators
models = dict(
    small=(G_small.to_json(), P_U_small, D.to_json()),
    medium=(G_medium.to_json(), P_U_medium, D.to_json()),
    large=(G_large.to_json(), P_U_large, D.to_json())
)

for model_size in models: # 3 possible
    for m in m_values: # 6 possible
        for P_X in data_distributions: # 3 possible
            # this inner loop runs 3*3*6 = 54 times.

            # create the name of the current configuration
            setup_name = "disc-{}-{}-m{}".format(model_size, P_X["fnc_name"], m)
            
            # set the hyper parameters as basically the same over all configurations, except the batchsize B
            # which we set B = m
            _lambda = dict(
                eta_g = 5e-3,
                eta_d = 5e-3,
                N = 5000,
                K = 100,
                B = m
            )

            # create the actual json config
            config = dict()
            config["name"] = setup_name
            # outer loop
            config["G"] = models[model_size][0]
            config["P_U"] = models[model_size][1]
            config["D"] = models[model_size][2]
            # middle loop
            config["m"] = m
            # inner loop
            config["P_X"] = P_X
            # hyperparameters
            config["lambda"] = _lambda

            with open("configs/{}.json".format(setup_name), "w") as fp:
                json.dump(config, fp, indent=2)