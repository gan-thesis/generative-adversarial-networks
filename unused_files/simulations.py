import json
import os
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import time
import matplotlib.pyplot as plt

import tensorflow as tf

from gan import GenerativeAdversarialNetwork
from training import ThesisAlgorithm
from utils.dist import get_distribution
import utils.store as store


def run(config, R):
    # the model-space for the generator
    G = tf.keras.models.model_from_json(config["G"])

    # the model-space for the discriminator
    D = tf.keras.models.model_from_json(config["D"])

    # the distribution P^U
    P_U = get_distribution(**config["P_U"])

    # the distribution P^X
    P_X = get_distribution(**config["P_X"])

    # the trainingalgorithm initalized with hyperparameters
    A = ThesisAlgorithm(**config["lambda"])

    # create data X
    X = P_X(config["m"])

    # define the GAN as a tuple (D,G,P,A)
    gan = GenerativeAdversarialNetwork(D, G, P_U, A)

    # Run training in a way, that we can cancel it anytime.
    try:
        gan.train(X)
    except KeyboardInterrupt:
        pass

    # Store the full setup.
    store.setup(D, G, config["P_U"], config["P_X"], config["lambda"], config["m"], path="evaluation/{}/{}/".format(config["name"], R))

    # Store the initial data
    store.data(X, path="evaluation/{}/{}/".format(config["name"], R))

    # Store informations about the training-process
    store.training(A, path="evaluation/{}/{}/".format(config["name"], R))

    # Store models
    store.models(gan, path="evaluation/{}/{}/".format(config["name"], R))

    # Store samples of the trained Generator
    store.samples(gan.G, P_U, path="evaluation/{}/{}/".format(config["name"], R))

    plt.plot(A.log["R_g"])
    plt.savefig("sample.png")

config_path = "configs/"
configs = os.listdir(config_path)

repetitions = {
    "10": 10,
    "25": 7,
    "50": 5,
    "75": 3,
    "100": 2,
    "1000": 1
}

start = time.time()
for config_file in configs:
    # if "disc" not in config_file: continue
    # # if ("m100" not in config_file) and ("m50" not in config_file): continue
    # if "small" not in config_file: continue
    # if "m1000" not in config_file: continue
    if "test-large" not in config_file: continue
    if os.path.isdir("evaluation/"+config_file.replace(".json","")):
        print("Skipping", config_file)
        continue
    print("Running", config_file)
    # continue
    # continue
    # read the config file
    with open(config_path+config_file) as fp:
        config = json.load(fp)
    # run the config
    # for R in range(1, repetitions[str(config["m"])]+1):
    run(config, 1)
end = time.time()-start

print("Total time of {}s".format(round(end)))